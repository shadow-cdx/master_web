const gulp = require('gulp');
const browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// 独立前端服务
function serveTask() {
    browserSync.init({
		//server: './',
		server: {
			baseDir: './'
		},
		port: 3000,
	});
    // watch for changes
    gulp.watch([
		'app/**/*.html',
		'app/**/*.css',
		'app/**/*.js',
		'*.html'
	  ]).on('change', reload);
}

exports.serve = serveTask; // 全栈开发前端服务


